package com.imamfarisi.sharedelement;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_layout);

        ImageView imgDetails = findViewById(R.id.imgDetail);
        TextView txtName = findViewById(R.id.txtName);

        imgDetails.setImageDrawable(getDrawable(getIntent().getIntExtra("gambar", R.mipmap.ic_launcher)));
        txtName.setText(getIntent().getStringExtra("judul"));
    }
}
